#
# Be sure to run `pod lib lint JHDPagesKit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |spec|
  spec.name              = 'JHDPagesKit'
  spec.version        = "1.0.2"
  spec.summary        = "Framework for https://pages.appstoreserver.de"
  spec.ios.deployment_target = '14.0'
  spec.osx.deployment_target  = '10.14'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  spec.description  = <<-DESC
  Framework for https://pages.appstoreserver.de
                   DESC

  spec.homepage            = 'https://bitbucket.org/appstoreserver/jhdpageskit'
  spec.license             = { :type => 'No License', :file => 'LICENSE' }

  spec.author              = { 'Jan-Hendrik' => 'jan.damerau@gmail.com' }

  spec.source              = { :git => 'https://appstoreserver@bitbucket.org/appstoreserver/jhdpageskit.git', :tag => spec.version.to_s }


  spec.source_files  = 'JHDPagesKit/Classes/**/*.{h,m,swift,xib}'
  spec.frameworks     = 'WebKit'
  spec.ios.framework  = 'UIKit'
  spec.osx.framework  = 'Cocoa'
   
  # spec.dependency 'Charts'
  # spec.dependency 'Alamofire'
  # spec.dependency 'AlamofireImage'
end
