//
//  AppDelegate.swift
//  Swift-OSX-Example
//
//  Created by Jan-Hendrik Damerau on 29.11.20.
//

import Cocoa
import JHDPagesKit

@main
class AppDelegate: NSObject, NSApplicationDelegate {
    
    var isDark: Bool { NSApp.effectiveAppearance.name == NSAppearance.Name.darkAqua }
    
    @IBOutlet var window: NSWindow!

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        
        // Insert code here to initialize your application
        let style: NSAppearance.Name = isDark ? .darkAqua : .vibrantLight
        
        JHDPages.userInterface(style: style)
        JHDPages.title(isHidden: false)
    }
    
    @IBAction func demoClicked(_ sender: NSButton) {
        JHDPages.present(handle: "demo_page")
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }
}

