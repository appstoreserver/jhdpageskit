//
//  ViewController.swift
//  Swift-iOS-Example
//
//  Created by Jan-Hendrik Damerau on 28.11.20.
//

import UIKit
import JHDPagesKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func demoClicked(_ sender: UIButton) {
        JHDPages.present(handle: "demo_page")
    }
}

