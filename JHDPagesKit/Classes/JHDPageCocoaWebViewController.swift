//
//  JHDPageCocoaWebViewController.swift
//
// Copyright (c) 2020 Jan-Hendrik Damerau (https://bitbucket.org/appstoreserver/jhdpageskit/)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
#if os(OSX)

import Cocoa
import WebKit

class JHDPageCocoaWebViewController: NSViewController, WKNavigationDelegate {
    
    // MARK: -
    // MARK: PUBLIC Interface
    // MARK: -
    // MARK: Properties
    
    /// Show/Hides the title label at toolbar
    public var titleIsHidden: Bool
    /// This is for overriding the title text from weppage
    public var titleText: String
    /// This is the background color during loading the webview
    public var backgroundColor: NSColor?
    
    // MARK: -
    // MARK: PRIVATE Interface
    // MARK: -
    // MARK: Properties
    
    private var url: URL
    private let webView = WKWebView()
    
    /// Initializer
    init(url: URL, titleIsHidden: Bool, title: String? = nil, backgroundColor: NSColor? = nil) {
        self.url = url
        self.titleIsHidden = titleIsHidden
        self.titleText = title ?? ""
        self.backgroundColor = backgroundColor
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
       fatalError()
    }
    
     override func loadView() {
         view = webView
     }

     override func viewDidLoad() {
        super.viewDidLoad()
        
        /// Webview style & request
        if let backgroundColor = self.backgroundColor {
            self.view.layer?.backgroundColor = backgroundColor.cgColor
            webView.setValue(false, forKey: "drawsBackground")
        }
        webView.navigationDelegate = self
        let request = URLRequest(url: self.url)
        webView.load(request)
     }
    
    override func viewDidAppear() {
        super.viewDidAppear()
        /// customize the title
        self.view.window?.title = self.titleIsHidden ? "" : self.titleText
    }
    
    /// Delegate method is used to get the weppage title to show it at toolbar
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if let title = webView.title {
            /// customize the title
            print("[JHDPages] loaded page with title: \"\(title)\"")
            self.view.window?.title = self.titleIsHidden ? "" : ((!self.titleText.isEmpty) ? self.titleText : title)
        }
    }
    
    /// By default a WKWebView can navigate to any links the user selects, but it’s common to want to restrict that.
    /// Allows the user to tap links and follow these links to the new weppage
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        switch navigationAction.navigationType {
            case .linkActivated:
                webView.load(navigationAction.request)
            default: break
        }
        decisionHandler(.allow)
    }
}

#endif
