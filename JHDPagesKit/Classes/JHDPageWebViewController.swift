//
//  JHDPageWebViewController.swift
//
// Copyright (c) 2020 Jan-Hendrik Damerau (https://bitbucket.org/appstoreserver/jhdpageskit/)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
#if os(iOS)

import UIKit
import WebKit

class JHDPageWebViewController: UIViewController, WKNavigationDelegate {
    
    // MARK: -
    // MARK: PUBLIC Interface
    // MARK: -
    // MARK: Properties
    
    /// This is the required request for the webview
    public var request: URLRequest!
    /// Show/Hides the title label at toolbar
    public var titleIsHidden: Bool
    /// This is the title text color at toolbar
    public var titleColor: UIColor
    /// This is for overriding the title text from weppage
    public var titleText: String
    /// This is the background color during loading the webview
    public var backgroundColor: UIColor
    /// This is the closing X image top-left at toolbar
    public var image: UIImage? {
          didSet {
              closeBtnImage = image!
          }
    }
    
    // MARK: -
    // MARK: PRIVATE Interface
    // MARK: -
    // MARK: Properties
    
    /// This is the container for embedding the hole view
    private lazy var viewContainer = UIView(frame: CGRect.zero)
    /// This is the final webview
    public private(set) lazy var webView: WKWebView = WKWebView(frame: CGRect.zero)
    /// Stores the last position of webview for pangesture
    private var lastPosition: CGPoint = .zero
    
    /// Custom Toolbar for the webview for closing the
    /// modal button and webpage title
    private lazy var toolbar:UIView = {
        /// Creates the toolbar with a common height of 44
        let v = UIView(frame: CGRect.zero)
        v.isUserInteractionEnabled = true
        v.heightAnchor.constraint(equalToConstant: 44.0).isActive = true
        v.translatesAutoresizingMaskIntoConstraints = false
        /// Add blurring effect for nicer view
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        v.addSubview(blurEffectView, stretchToFit: true)
        return v
    }()
    
    /// Image for the closing Button
    private lazy var closeBtnImage: UIImage = {
        let img: UIImage = UIImage(systemName: "xmark.circle")!
        return img
    }()
    
    /// The title label at toolbar
    private lazy var titleLabel:UILabel = {
        let lbl = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 264.0, height: 16.0))
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 0.5
        lbl.textAlignment = .center
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        return lbl
    }()

    /// Initializer
    init(titleColor: UIColor, titleIsHidden: Bool, title: String? = nil, backgroundColor: UIColor, closeBtnImage: UIImage? = nil) {
        self.titleColor = titleColor
        self.titleIsHidden = titleIsHidden
        self.titleText = title ?? ""
        self.backgroundColor = backgroundColor
        super.init(nibName: nil, bundle: nil)
        if let image = closeBtnImage {
            self.image = image
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        /// Set a gesture for swipe down effect
        addPanGestureRecognizer()
        /// customize the title
        titleLabel.text = self.titleText
        titleLabel.textColor = self.titleColor
        titleLabel.isHidden = self.titleIsHidden
        /// Webview style & request
        webView.navigationDelegate = self
        webView.backgroundColor = self.backgroundColor
        webView.isOpaque = false
        webView.load(request)
    }

    /// Hey webview, do your stuff and run :-)
    override public func loadView() {
        super.loadView()
        setupLayout()
    }
    
    /// Set the layout. At top a toolbar and underneath the webview container with the webpage
    /// Currently there is no progressbar.
    private func setupLayout() {
        
        /// Creates the button here, due to the custom image might be set
        let closeButton = UIButton(type: .custom)
        closeButton.setImage(closeBtnImage.withRenderingMode(.alwaysTemplate), for: .normal)
        closeButton.tintColor = titleColor
        closeButton.addTarget(self, action: #selector(dismissWebview(_:)), for: .touchUpInside)
        closeButton.tintColor = titleColor
        closeButton.widthAnchor.constraint(equalTo: closeButton.heightAnchor).isActive = true
        
        /// Creates the toolbar
        let toolbarStackView = UIStackView(arrangedSubviews: [closeButton, titleLabel])
        toolbarStackView.spacing = 2.0
        toolbarStackView.axis = .horizontal
        toolbar.addSubview(toolbarStackView)
        toolbarStackView.translatesAutoresizingMaskIntoConstraints = false
        toolbarStackView.topAnchor.constraint(equalTo: toolbar.topAnchor, constant: 5).isActive = true
        toolbarStackView.leadingAnchor.constraint(equalTo: toolbar.leadingAnchor, constant: 5).isActive = true
        toolbarStackView.bottomAnchor.constraint(equalTo: toolbar.bottomAnchor, constant: -5).isActive = true
        toolbarStackView.trailingAnchor.constraint(equalTo: toolbar.trailingAnchor, constant:  -49).isActive = true
        
        /// Creates the webview
        view = UIView()
        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        view.backgroundColor = .clear
        view.addSubview(viewContainer)
        viewContainer.translatesAutoresizingMaskIntoConstraints = false
        viewContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        viewContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        viewContainer.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        viewContainer.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        viewContainer.layer.cornerRadius = 16.0
        viewContainer.clipsToBounds = true
        
        /// All together
        let mainStackView = UIStackView(arrangedSubviews: [toolbar, webView])
        mainStackView.axis = .vertical
        viewContainer.addSubview(mainStackView, stretchToFit: true)
    }
    
    /// Action called by closing X button
    @objc private func dismissWebview(_ sender: UIButton) {
        dismiss(completion: nil)
    }
    
    /// Dismiss the view
    /// Discussion: Is a completion handler required? May I implement it later.
    public func dismiss(completion: (() -> Void)? = nil) {
        dismiss(animated: true, completion: completion)
    }
    
    /// Delegate method is used to get the weppage title to show it at toolbar
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if let title = webView.title {
            print("[JHDPages] loaded page with title: \"\(title)\"")
            titleLabel.text = (!self.titleText.isEmpty) ? self.titleText : title
        }
    }
    
    /// By default a WKWebView can navigate to any links the user selects, but it’s common to want to restrict that.
    /// Allows the user to tap links and follow these links to the new weppage
    public func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        switch navigationAction.navigationType {
            case .linkActivated:
                webView.load(navigationAction.request)
            default: break
        }
        decisionHandler(.allow)
    }
}

/// UIGestureRecognizerDelegate conformance
extension JHDPageWebViewController: UIGestureRecognizerDelegate {
    
    /// Add the gesture for swiping down, which the user expects when a modal was opened
    fileprivate func addPanGestureRecognizer() {
        let panRecognizer = UIPanGestureRecognizer(target: self,action: #selector(self.handlePanning(_:)))
        panRecognizer.delegate = self
        panRecognizer.maximumNumberOfTouches = 1
        panRecognizer.minimumNumberOfTouches = 1
        panRecognizer.cancelsTouchesInView = true
        toolbar.gestureRecognizers?.forEach { $0.require(toFail: panRecognizer) }
        toolbar.gestureRecognizers = [panRecognizer]
    }
    
    /// Handles the swioe down functionallity for the user
    @objc private func handlePanning(_ gestureRecognizer: UIPanGestureRecognizer?) {
        
        switch gestureRecognizer?.state {
            case .began:
                lastPosition = viewContainer.center
                self.setLastPosition(gestureRecognizer: gestureRecognizer)
            case .ended:
                /// Close the view if more than a half was swiped down
                if viewContainer.frame.origin.y > view.frame.size.height/2.0 {
                    self.dismiss()
                    return
                }
                /// Remove to previous position, if it was not moved half down
                self.setLastPosition(gestureRecognizer: gestureRecognizer)
                UIView.animate(withDuration: 0.7, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.5, options: .allowUserInteraction, animations: {
                    self.viewContainer.center = self.lastPosition
                })
            case .cancelled: break
            default:
                /// Set position in all cases, if not .began, .ended or .cancelled,
                self.setLastPosition(gestureRecognizer: gestureRecognizer)
                break
        }
    }
    
    /// Set the position for the view
    func setLastPosition(gestureRecognizer: UIPanGestureRecognizer?) {
        guard let translation: CGPoint = gestureRecognizer?.translation(in: view) else { return }
        viewContainer.center = CGPoint(x: viewContainer.center.x,y: lastPosition.y + translation.y)
    }
}


public extension UIView {
    ///This extension extends the "subview" with settings constraints to
    /// strecth a view fin in size of parent
    func addSubview(_ subview: UIView, stretchToFit: Bool = false) {
        addSubview(subview)
        if stretchToFit {
            subview.translatesAutoresizingMaskIntoConstraints = false
            leftAnchor.constraint(equalTo: subview.leftAnchor).isActive = true
            rightAnchor.constraint(equalTo: subview.rightAnchor).isActive = true
            topAnchor.constraint(equalTo: subview.topAnchor).isActive = true
            bottomAnchor.constraint(equalTo: subview.bottomAnchor).isActive = true
        }
    }
}

#endif
