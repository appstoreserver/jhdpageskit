//
//  JHDPages.swift
//
// Copyright (c) 2020 Jan-Hendrik Damerau (https://bitbucket.org/appstoreserver/jhdpageskit/)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

import Foundation
import WebKit

#if os(iOS)
    import UIKit
#elseif os(OSX)
    import Cocoa
#else
    /// Not yet supported
#endif

@objc public enum JHDPagesAppearance: Int {
    case light = 1
    case dark = 2
}

@objc public class JHDPages : NSObject {
    // MARK: -
    // MARK: PUBLIC Interface
    // MARK: -
    // MARK: Properties
#if os(iOS)
    public typealias Font = UIFont
    public typealias Color = UIColor
    public typealias InterfaceStyle = UIUserInterfaceStyle
#elseif os(OSX)
    public typealias Font = NSFont
    public typealias Color = NSColor
    public typealias InterfaceStyle = NSAppearance.Name
#else
    // Untested, and currently unsupported
#endif
    
    /// Set your Apple UIUserInterfaceStyle.
    /// This is a optional setup value, which can changed at anytime, for e.g.
    ///
    ///     if let style = window?.traitCollection.userInterfaceStyle {
    ///         JHDPages.userInterface(style: style)
    ///     }
    ///
    /// The default is UIUserInterfaceStyle.light
    /// - Parameter style: UIUserInterfaceStyle
    ///
    public class func userInterface(style: InterfaceStyle) {
        #if os(iOS)
        let s:JHDPagesAppearance = (style == .dark) ? . dark : .light
        JHDPagesManager.defaultManager.userInterfaceStyle = s
        #elseif os(OSX)
        let s:JHDPagesAppearance = (style == .darkAqua || style == .vibrantDark) ? . dark : .light
        JHDPagesManager.defaultManager.userInterfaceStyle = s
        #endif
        print("[JHDPages] will present all pages in \((s == .dark) ? "darkmode" : "lightmode")")
    }
    
    /// Set your title color.
    /// This is a optional setup value, which can changed at anytime.
    /// The default is black
    ///
    /// - Parameter color: UIColor, NSColor
    ///
    public class func title(color: Color) {
        JHDPagesManager.defaultManager.titleColor = color
    }
    
    /// Display/Hide the title.
    /// This is a optional setup value, which can changed at anytime.
    /// The default is TRUE
    ///
    /// - Parameter isHidden: Bool
    ///
    public class func title(isHidden: Bool) {
        JHDPagesManager.defaultManager.titleIsHidden = isHidden
    }
    
    /// Set your background color, which occurs during loading.
    /// This is a optional setup value, which can changed at anytime.
    /// The default is white
    ///
    /// - Parameter color: UIColor, NSColor
    ///
    public class func background(color: Color) {
        JHDPagesManager.defaultManager.backgroundColor = color
    }
    
    #if os(iOS)
    /// Customize the closing X Button for iOS
    /// By default is a circle x image displayed
    ///
    /// - Parameter image: UIImage
    ///
    public class func button(image: UIImage) {
        JHDPagesManager.defaultManager.buttonImage = image
    }
    #endif
    
    // MARK: Events
    ///
    /// Tells JHDPages that the user performed a webview request.
    /// This method loads the webview for the given handle and will present it modally.
    /// The UIUserInterfaceStyle appearance can be overriden here once, for any special cases.
    ///
    /// - Parameter handle: String
    /// - Parameter title: String
    /// - Parameter style: JHDPagesAppearance
    ///
    public class func present(handle: String, title: String? = nil, style: JHDPagesAppearance? = nil) {
        JHDPagesManager.defaultManager.present(handle: handle, title: title, style: style)
    }
    
    /// Tells JHDPages that the user performed a webview request.
    /// This method loads the webview for the given url and will present it modally.
    ///
    /// - Parameter url: String
    /// - Parameter title: String
    ///
    public class func present(url: String, title: String? = nil) {
        if let url = URL(string: url) {
            JHDPagesManager.defaultManager.present(url: url, title: title)
        }
    }
}

// MARK: -
// MARK: PRIVATE Interface

/// Define the bundle to get acces to its assets
public final class JHDPagesBundle: NSObject {
    public static let bundle = Bundle(for: JHDPagesBundle.self)
}

#if os(iOS)
    @objc private class JHDPagesBase : NSObject { }
    public typealias Font = UIFont
    public typealias Color = UIColor
    public typealias Image = UIFont
#elseif os(OSX)
    @objc private class JHDPagesBase : NSObject { }
    public typealias Font = NSFont
    public typealias Color = NSColor
    public typealias Image = NSImage
#else
    // Untested, and currently unsupported
#endif

@objc private class JHDPagesManager : JHDPagesBase {
    
    /// This is the target URL for teh webview. There is nothing magic,
    /// it is a public domain
    fileprivate let targetUrl: String = "https://pages.appstoreserver.de/"
    
    /// This is the required appaerance style for the webview. By default it is light,
    /// this value can be set globally or even for each request.
    fileprivate var userInterfaceStyle: JHDPagesAppearance = .light
    
    /// This is the required title color for the webview. By default it is black,
    /// this value can be set globally.
    fileprivate var titleColor: Color = .black
    
    /// This is the required appaerance style for hiproperty to hide/display the title.
    /// By default it is displayed, this value can be set globally.
    fileprivate var titleIsHidden: Bool = false
    
    /// This is the required background color for the webview during loading.
    /// By default it is white, this value can be set globally.
    fileprivate var backgroundColor: Color = .white
    
    #if os(iOS)
    /// Customize the closing X Button for iOS
    /// By default is a circle x image displayed
    fileprivate var buttonImage: UIImage?
    #endif
    
    /// This is method contains the Tracking Logic / Configuration reading from Info.plist
    fileprivate var token: String? {
        if let pagesToken = Bundle.main.object(forInfoDictionaryKey: "PagesToken") as? String {
            return pagesToken
        }
        print("[JHDPages] The \"PagesToken\" has to be set first as a String at Info.plist.")
        return nil
    }
    
    /// This is method returns teh required string for the URL with the information
    /// which appearance is required
    private func getAppearanceForURL(style: JHDPagesAppearance? = nil) -> String {
        var appearanceMode = (self.userInterfaceStyle == .dark) ? "dark" : "light"
        if let appearance = style {
            appearanceMode = (appearance == .dark) ? "dark" : "light"
        }
        return appearanceMode
    }
    
    /// This method is called to open the view modally. The URL is the needed param
    /// to have a successfull webview. This method does not need an account.
    fileprivate func present(url: URL, title: String? = nil) {
        self.present(url: url, title: title, style: nil)
    }
    
    /// This method is called to open the view modally. The handle is the needed param
    /// to have a successfull webview. The handle can be found at the administration
    /// dashboard at your account site. If you do not have an account yet, ask the
    /// developer or jandamerau.com
    fileprivate func present(handle: String, title: String? = nil, style: JHDPagesAppearance? = nil) {
        
        guard let token = self.token else { return }
        print("[JHDPages] will open page for handle: \"\(handle)\"")

        let appearanceMode = self.getAppearanceForURL(style: style)
        if let url = URL(string: "\(self.targetUrl)/Display/For/App/\(token)/handle/\(handle)/appearance/\(appearanceMode)") {
            self.present(url: url, title: title, style: style)
        }
    }
    
    /// This method is called to open the view modally. The handle is the needed param
    /// to have a successfull any webview by given URL.
    fileprivate func present(url: URL, title: String? = nil, style: JHDPagesAppearance? = nil) {
        
        #if os(iOS)
            if let viewController = self.lifecycleViewController() {
                let urlRequest = URLRequest(url: url)
                let web = JHDPageWebViewController(titleColor: titleColor,
                                                   titleIsHidden: titleIsHidden,
                                                   title: title,
                                                   backgroundColor: backgroundColor)
                web.request = urlRequest
                web.modalPresentationStyle = .overCurrentContext
                viewController.present(web, animated: true, completion: nil)
            }
        #elseif os(OSX)
            let size = CGSize.init(width: NSScreen.main!.frame.midX, height: NSScreen.main!.frame.midY)
            let rect = CGRect.init(origin: .zero, size: size)
            let styleMask: NSWindow.StyleMask = [.closable, .miniaturizable, .resizable, .titled]
        
            let newWindow = NSWindow(contentRect: rect, styleMask: styleMask, backing: .buffered, defer: false)
            newWindow.isOpaque = false
            newWindow.center()
            newWindow.isMovableByWindowBackground = true
            newWindow.backgroundColor = self.backgroundColor
            newWindow.makeKeyAndOrderFront(nil)
            newWindow.isReleasedWhenClosed = false
        
            let frame = CGRect(origin: .zero, size: CGSize(width: 640, height: 480))
            let viewController = JHDPageCocoaWebViewController(url: url!,
                                                               titleIsHidden: self.titleIsHidden,
                                                               title: title,
                                                               backgroundColor: backgroundColor)
            viewController.view.frame = frame
            newWindow.contentViewController = viewController
        #endif
    }
    /// This method get the presenting controller from lifecycle
    /// for presenting the webviev to the user.
    #if os(iOS)
    fileprivate  func lifecycleViewController(base: UIViewController? = UIApplication.shared.delegate?.window??.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return lifecycleViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return lifecycleViewController(base: selected)
        }
        if let presented = base?.presentedViewController {
            return lifecycleViewController(base: presented)
        }
        if UIApplication.shared.connectedScenes.count > 0 {
            for scene in UIApplication.shared.connectedScenes {
                if scene.activationState == .foregroundActive {
                    if let sceneVC = ((scene as? UIWindowScene)?.delegate as? UIWindowSceneDelegate)?.window!!.rootViewController {
                        return sceneVC
                    }
                }
            }
        }
        return base
    }
    #endif
    
    #if os(iOS)
    
    #endif

    // MARK: -
    // MARK: Singleton
    public class var defaultManager: JHDPagesManager {
        struct Singleton {
            static let instance: JHDPagesManager = JHDPagesManager()
        }
        return Singleton.instance
    }
    
    override init() { super.init() }
}
