# JHDPagesKit

This Pod is the extension for [https://pages.appstoreserver.de](https://pages.appstoreserver.de)
At your [account](https://pages.appstoreserver.de) you can add unlimited pages for your app. 
There is a full markdown syntax supported. 
Each page got an handle. 
With this handle you can popup your pages in your app.
This makes the handling for Privacy Policys, Legal Agreements, License pretty easy.

![iOS Account Preview](https://bitbucket.org/appstoreserver/jhdpageskit/raw/5a6a7191fe6e7afc8dc48e2e1c95650e6c198ba0/README_Preview.png)

You can use the  [demo login](https://pages.appstoreserver.de) for testings:

```bash
Email: hello@example.com
Password: demo
```


## Screenshots

A fully integrated page is a webview with your content from [https://pages.appstoreserver.de](https://pages.appstoreserver.de). Here comes an example how it can be integrated.

![iOS Example Shots](https://bitbucket.org/appstoreserver/jhdpageskit/raw/79d64b850ee60126566c34e579d6ecb5a241e645/README_Example.gif)

## Requirements

- Xcode 11.0+
- iOS 14.0+, Mac OS X 10.14+

## Installation

[CocoaPods](http://cocoapods.org) is a dependency manager for Cocoa projects. You can install it with the following command:

```bash
$ gem install cocoapods
```
> CocoaPods 0.39.0+ is required to build JHDPagesKit.

To integrate JHDPagesKit into your Xcode project using CocoaPods, specify it in your `Podfile`:

```ruby
pod 'JHDPagesKit', :git => 'https://bitbucket.org/appstoreserver/jhdpageskit.git'
```

Then, run the following command:

```bash
$ pod install
```

You can add PagesToken `68650801-B21C-46D5-AE45-EC8BB361EB6B` to `Info.plist` and use the handle `demo_page` for call a page.

In your  `Info.plist` add following:
```bash
<key>PagesToken</key>
<string>68650801-B21C-46D5-AE45-EC8BB361EB6B</string>
<key>NSAppTransportSecurity</key>
<dict>
    <key>NSAllowsArbitraryLoads</key>
    <true/>
</dict>
```
**Cocoa**: You might enable `Incoming Connections` and  `Outgoing Connections` at your `Signing & Capabilities`.

In your `AppDelegate` add:

```swift
/// iOS Example
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
    if let style = window?.traitCollection.userInterfaceStyle {
        JHDPages.userInterface(style: style)
    }
    
    return true
}
```

```swift
/// OSX Example
var isDark: Bool { NSApp.effectiveAppearance.name == NSAppearance.Name.darkAqua }

func applicationDidFinishLaunching(_ aNotification: Notification) {
    let style: NSAppearance.Name = isDark ? .darkAqua : .vibrantLight

    JHDPages.userInterface(style: style)
}
```


```swift
   JHDPages.present(handle: "demo_page")
```

### Presenting

For presenting you need the `handle` (at demo you can use above values).  

Run somewhere in your code 
```swift
   JHDPages.present(handle: "demo_page")
```
You can also manipulate each event as follows:

```swift
   JHDPages.present(handle: "demo_page", 
                    title: "My demo title", 
                    style: .dark)
```

In general you can also present any URL without any token or account

```swift
    JHDPages.present(url: "http://www.example.com")
    JHDPages.present(url: "http://www.example.com", title: "My demo title")
```

### Layouting / Customization

Title Color (iOS)
```swift
/// iOS
JHDPages.title(color: UIColor)
/// OSX
JHDPages.title(color:NSColor)
```
Title Hiding (iOS OSX)
```swift
JHDPages.title(isHidden: Bool)
```

Background color, which occurs during loading (iOS OSX)
```swift
/// iOS
JHDPages.background(isHidden: UIColor)
/// Cocoa
JHDPages.background(isHidden: NSColor)
```

Closing Button (iOS)
```swift
/// iOS
JHDPages.button(image: UIImage)
```
You can check also the iOS and/or Cocoa example projects.

## Author

Jan-H. Damerau <app@jandamerau.com>

## License

JHDPagesKit is released under an MIT license. See [LICENSE](https://bitbucket.org/appstoreserver/jhdpageskit/src/master/LICENSE) for more information.
